import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Loader from '../Components/Loader'
import ProductCard from '../Components/ProductCard'

function Home(){

    const url = `http://localhost:8081/api/products`
    const [products, setProduct] = useState({
        loading: false,
        data: null 
    })

    let content = null

    useEffect(() => {
        setProduct({
            loading: true,
            data: null,
            error: false
        })
        axios.get(url)
            .then(response => {
                setProduct({
                    loading: false,
                    data: response.data,
                    error: false
                })
            })
            .catch(error => {
                setProduct({
                    loading: false,
                    data: null,
                    error: true
                })
            })
    }, [url])

    if (products.error){
        content = <div>Product Not Found ):</div>
    }

    if (products.loading){
        content = <Loader></Loader>
    }

    if(products.data){
        content = 
        products.data.map((product, key) =>
            <div key={product.id} className="p-2">
                <ProductCard
                    product={product}
                />
            </div>
        )
    }

    return (
        <div className="container mx-auto">
            <h1 className="font-bold text-2xl mb-3">
                Best Sellers
            </h1>
            <div className="md:flex flex-wrap md:-mx-3">
                { content } 
            </div>
        </div>
    )
}

export default Home