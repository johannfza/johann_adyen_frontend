import React, { useState, useEffect} from 'react'
import axios from 'axios'
import { useParams } from 'react-router-dom'
import { Button, Alert } from "react-bootstrap"
import NumberFormat from 'react-number-format';
import Loader from '../Components/Loader'


function Product(){

    const [showAlert, setShowAlert] = useState(false);
    const [showErrorAlert, setShowErrorAlert] = useState(false);

    const cartId = 'c10001'

    const { id } = useParams()

    const url = `http://localhost:8081/api/products/${id}`

    const addToCartUrl = `http://localhost:8082/api/carts/items/add/${cartId}`

    const [product, setProduct] = useState({
        loading: false,
        data: null 
    })

    let alert = null
    let content = null

    useEffect(() => {
        setProduct({
            loading: true,
            data: null,
            error: false
        })
        axios.get(url)
            .then(response => {
                setProduct({
                    loading: false,
                    data: response.data,
                    error: false
                })
            })
            .catch(error => {
                setProduct({
                    loading: false,
                    data: null,
                    error: true
                })
            })
    }, [url])

    function addToCart() {
      axios.post(addToCartUrl, {
        headers:{
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, DELETE, PUT',
          'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token',
          'crossorigin' : 'true'
        },
        "productId": id,
        "qty": 1
      })
      .then(function (reponse){
        setShowAlert(true)
      })
      .catch(function (error){
        setShowErrorAlert(true)
      })
    }

    if (showAlert){
        alert = <Alert variant="success" onClose={() => setShowAlert(false)} dismissible>
          {product.data.name} added to Cart!
        </Alert>
    }

    if (showErrorAlert){
        alert = <Alert variant="danger" onClose={() => setShowErrorAlert(false)} dismissible>
          Something went wrong, Unable to add {product.data.name} to Cart!
        </Alert>
    }

    if (product.error){
        content = <div>Product Not Found ):</div>
    }

    if (product.loading){
        content = <Loader></Loader>
    }
    
    if (product.data) {
      content = (
        <div>
          <h1 className="text-2xl font-bold mb-3">{product.data.name}</h1>
          <div>
            <img
              src={require(`../assets/imgUrl/${product.data.id}.jpg`)}
              alt={product.data.name}
              className="h-64"
            />
          </div>
          <NumberFormat className="font-bold text-xl mb-3" displayType={'text'} value={product.data.price} prefix={'$'} decimalScale="2" fixedDecimalScale="true" />
          <div>{product.data.desc}</div>

          <Button className="mb-3" variant="secondary" onClick={() => addToCart()}>Add to Cart</Button>
        </div>
      )
    }

  return <div className="container mx-auto">{alert}{content}</div>
}

export default Product