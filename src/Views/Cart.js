import React, { useState, useEffect} from 'react'
import axios from 'axios'
//import { useParams } from 'react-router-dom'
import { Table, Button} from "react-bootstrap"
import NumberFormat from 'react-number-format';
import Loader from '../Components/Loader'
import CartItem from '../Components/CartItem'
import { Link } from "react-router-dom"

function Cart(props){

    const id = 'c10001'

    const url = `http://localhost:8082/api/carts/${id}`

    let content = null
    let items = null

    const [cart, setCart] = useState({
        loading: false,
        data: null 
    })

    function updateCart(){
                axios.get(url)
            .then(response => {
                setCart({
                    loading: false,
                    data: response.data,
                    error: false
                })
            })
            .catch(error => {
                setCart({
                    loading: false,
                    data: null,
                    error: true
                })
            })
        }
    

    function handleRemove(id){
        console.log(id)
        axios.delete(`http://localhost:8082/api/carts/remove/item/${id}`)
        .then( response => {
            updateCart()
        })
        .catch(error => {
            console.log('error')
        })
    }


    useEffect(() => {
        setCart({
            loading: true,
            data: null,
            error: false
        })
        axios.get(url)
            .then(response => {
                setCart({
                    loading: false,
                    data: response.data,
                    error: false
                })
            })
            .catch(error => {
                setCart({
                    loading: false,
                    data: null,
                    error: true
                })
            })
    }, [url])

    if (cart.error){
        content = <div>Product Not Found ):</div>
    }

    if (cart.loading){
        content = <Loader></Loader>
    }
    
    if (cart.data) {
        if (cart.data.items.length < 1){
            items = (    
            <tr>
                <td colspan="6"><div className="text-center p-3">Your Cart is Empty<Link to="/" >Shop</Link></div></td>
            </tr> 
            )
        } else {
            items = 
                cart.data.items.map((item, key) => 
            <CartItem key={item.id} item={item} onRemove={handleRemove}/>  
        )
        }

      content = (
        <div>
          <h1 className="text-2xl font-bold mb-3">Cart</h1>
           <Table responsive>
               <thead>
                    <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Qty</th>
                        <th>SubTotal</th>
                        <th></th>
                    </tr>
               </thead>
                <tbody>
                    {items}
                </tbody>
           </Table>

           <div className="border-t p-3 flex justify-between items-center">
           <div className="font-bold text-xl mb-3">Total: &nbsp;<NumberFormat className="font-bold text-xl mb-3" displayType={'text'} value={cart.data.total} prefix={'$'} decimalScale="2" fixedDecimalScale="true" /></div>
           <Link to={`/checkout`}><Button>Check Out</Button></Link>
           </div>
        </div>
      )
    }

  return <div className="container mx-auto">{content}</div>

}

export default Cart