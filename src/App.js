import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom"
import './tailwind.css';
import Header from './Components/Header.js'
import Footer from './Components/Footer.js'
import Home from './Views/Home'
import About from './Views/About'
import Product from './Views/Product'
import Cart from './Views/Cart'
import Checkout from './Views/Checkout'
import Status from './Views/Status'


function App() {
  return (
    <div className="relative pb-10 min-h-screen">
      <Router>
        <Header />
        <div className="p-3">
          <Switch>
            <Route path="/status/:type">
          <Status />
            </Route>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/products/:id">
              <Product />
            </Route>
            <Route path="/cart">
              <Cart />
            </Route>
            <Route path="/checkout">
              <Checkout />
            </Route>
          </Switch>
        </div>
        <Footer />
      </Router>
    </div>
  )
}

export default App;
