import React from "react"
//import { Card, Button } from "react-bootstrap"
import NumberFormat from 'react-number-format'
import { Link } from "react-router-dom"

function CartItem(props) {

  return (
    <tr key={props.id}>
        <td><Link to={`/products/${props.item.product.id}`}><img className="h-20 w-20" src={require(`../assets/imgUrl/${props.item.product.id}.jpg`)} alt='small-product-logo'/></Link></td>
        <td>{props.item.product.name}</td>
        <td><NumberFormat displayType={'text'} value={props.item.product.price} prefix={'$'} decimalScale="2" fixedDecimalScale="true" /></td>
        <td>{props.item.qty}</td>
        <td><NumberFormat displayType={'text'} value={props.item.subTotal} prefix={'$'} decimalScale="2" fixedDecimalScale="true" /></td>
        <td onClick={() => props.onRemove(props.item.id)} >x</td>
    </tr>
  )
}

export default CartItem
