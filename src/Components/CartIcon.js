import React from 'react'
import { Link } from "react-router-dom"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'

function CartIcon(){
    return (
        <Link
            to="/cart" 
        >
            <span className="text-xl">
                <FontAwesomeIcon
                    icon={faShoppingCart}
                />
            </span>
        </Link>
    )
}

export default CartIcon