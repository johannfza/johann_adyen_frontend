import React, { Component } from "react"

export default class CheckoutContainer extends Component {
  constructor(props) {
    super(props)
    //
    this.paymentContainer = React.createRef()
    this.paymentComponent = null

    // Bind to on SubmitEvent
    this.onSubmit = this.onSubmit.bind(this)
    // Bind to on Additional Details post
    this.onAdditionalDetails = this.onAdditionalDetails.bind(this)
    // Bind to on payment reponse
    this.processPaymentResponse = this.processPaymentResponse.bind(this)
  }

  componentDidMount() {
    const link = document.createElement("link");
    link.rel = "stylesheet";
    link.href =
      "https://checkoutshopper-test.adyen.com/checkoutshopper/sdk/3.0.0/adyen.css";
    document.head.appendChild(link);

    const script = document.createElement("script");
    script.src =
      "https://checkoutshopper-test.adyen.com/checkoutshopper/sdk/3.0.0/adyen.js";
    script.async = true;
    script.onload = this.initAdyenCheckout; // Wait until the script is loaded before initiating AdyenCheckout
    document.body.appendChild(script);

    // GET Config from Server
    this.props.getAdyenConfig()
    // GET Payment Methods
    this.props.getPaymentMethods()
  }

  componentDidUpdate(prevProps) {

    const {
      paymentMethodsRes: paymentMethodsResponse,
      config,
      paymentRes,
      paymentDetailsRes,
      error
    } = this.props.payment
    if (error && error !== prevProps.payment.error) {
      window.location.href = `/status/error?reason=${error}`
      return
    }
    // 1. CREATING ADYEN DROP IN
    // if paymentMethodsResponse && Config exist Methods Exist
    // and not the same value and previous instance
    if (
      paymentMethodsResponse &&
      config &&
      (paymentMethodsResponse !== prevProps.payment.paymentMethodsRes ||
        config !== prevProps.payment.config)
    ) {
      // Create Adyen Chceckout and Mount
      // @ts-ignore
      // eslint-disable-next-line no-undef
      this.checkout = new AdyenCheckout({
        ...config,
        paymentMethodsResponse,
        onAdditionalDetails: this.onAdditionalDetails,
        onSubmit: this.onSubmit
      })

      this.checkout.create(this.props.type).mount(this.paymentContainer.current)
    }
    // 2A. PROCESS PAYMENT
    if (paymentRes && paymentRes !== prevProps.payment.paymentRes) {
      this.processPaymentResponse(paymentRes)
    }
    // 2A. PROCESS PAYMENT WITH ADDITIONAL DETAILS 
    if (
      paymentRes &&
      paymentDetailsRes !== prevProps.payment.paymentDetailsRes
    ) {
      this.processPaymentResponse(paymentDetailsRes)
    }
  }
  
  // Route to page using response form payment   
  processPaymentResponse(paymentRes) {
    if (paymentRes.action) {
      this.paymentComponent.handleAction(paymentRes.action)
    } else {
      switch (paymentRes.resultCode) {
        case "Authorised":
          window.location.href = "/status/success"
          break
        case "Pending":
          window.location.href = "/status/pending"
          break
        case "Refused":
          window.location.href = "/status/failed"
          break
        default:
          window.location.href = "/status/error"
          break
      }
    }
  }

    onSubmit(state, component) {
    const { billingAddress } = this.props.payment;
    if (state.isValid) {
      this.props.initiatePayment({
        ...state.data,
        billingAddress: this.props.type === "card" && billingAddress.enableBilling ? billingAddress : null,
        origin: window.location.origin
      });
      this.paymentComponent = component;
    }
  }

  onAdditionalDetails(state, component) {
    this.props.submitAdditionalDetails(state.data);
    this.paymentComponent = component;
  }

  render() {
        return (
          <div className="payment-container">
            <div ref={this.paymentContainer} className="payment"></div>
          </div>
        )
  }
}
