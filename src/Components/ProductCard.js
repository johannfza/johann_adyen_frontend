import React from "react"
import { Card, Button } from "react-bootstrap"
import NumberFormat from 'react-number-format'
import { Link } from "react-router-dom"

function ProductCard(props) {
  return (
    <Card className="mb-3" style={{ width: "18rem" }}>
      <Link to={`/products/${props.product.id}`}>
        <Card.Img
          variant="top"
          src={require(`../assets/imgUrl/${props.product.id}.jpg`)}
        />
      </Link>
      <Card.Body>
        <Link to={`/products/${props.product.id}`}>
          <Card.Title>{props.product.name}</Card.Title>
        </Link>

        <NumberFormat className="font-bold text-xl mb-3" value={props.product.price} prefix={'$'} decimalScale="2" fixedDecimalScale="true" />
        <Link to={`/products/${props.product.id}`}>
          <Button variant="secondary">View</Button>
        </Link>
      </Card.Body>
    </Card>
  )
}

export default ProductCard
