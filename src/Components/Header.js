import React from 'react'
import logo from '../assets/VinylVibes.png'
import Navigation from './Navigation.js'
import CartIcon from './CartIcon'
import { Link } from "react-router-dom"

function Header(){
    return (
        <header className="border-b p-3 flex justify-between items-center">
            <Navigation />
            <Link 
                to="/" 
            >
            <img className="h-10" src={logo} alt='small-logo'/>
            </Link>
            <CartIcon />
        </header>    
    )
}

export default Header