import { createSlice } from '@reduxjs/toolkit'

export const slice = createSlice({
  // Customer Billing Information
  name: "payment",
  initialState: {
    error: "",
    // Payments Methods Response 
    paymentMethodsRes: null,
    // Payment Response
    paymentRes: null,
    // Payment Details Response
    paymentDetailsRes: null,
    // Payment Config -- Reducer: config
    config: {
      paymentMethodsConfiguration: {
        ideal: {
          showImage: true
        },
        card: {
          hasHolderName: true,
          holderNameRequired: true,
          name: "Credit or debit card",
          amount: {
            value: 1000, // 10SGD
            currency: "SGD"
          }
        }
      },
      locale: "en_US",
      showPayButton: true
    },
    // Billling Details -- Reducer: SetBilling 
    billingAddress: {
      enableBilling: false,
      firstName: "Joe",
      lastName: "Bob",
      houseNumberOrName: "274",
      street: "Brannan Street",
      city: "San Francisco",
      stateOrProvince: "California",
      postalCode: "94107",
      country: "US"
    }
  },
  reducers: {
    // Set billingAddress fields
    setBilling: (state, action) => {
      state.billingAddress = {
        ...state.billingAddress,
        ...action.payload
      }
    },
    // Sets config paramenters
    config: (state, action) => {
      state.config = {
        ...state.config,
        ...action.payload
      }
    },
    // Sets amount 
    setAmount: (state, action) => {
      //state.config.paymentMethodsConfiguration.card.amount.value = 50000
      const [res, status] = action.payload;
      if (status >= 300) {
        state.error = res;
      } else {
        state.config.paymentMethodsConfiguration.card.amount = res;
      }
    },
    // Sends a POST request to server and sets paymentMethodRes with Response 
    paymentMethods: (state, action) => {
      const [res, status] = action.payload;
      if (status >= 300) {
        state.error = res;
      } else {
        state.paymentMethodsRes = res;
      }
    },
    // initiatePayment
    payments: (state, action) => {
      const [res, status] = action.payload;
      if (status >= 300) {
        state.error = res;
      } else {
        state.paymentRes = res;
      }
    }, 
    // submitAdditionalDetails
    paymentDetails: (state, action) => {
      const [res, status] = action.payload;
      if (status >= 300) {
        state.error = res;
      } else {
        state.paymentDetailsRes = res;
      }
    }
  }
})

export const { setBilling, setAmount, config, paymentMethods, payments, paymentDetails } = slice.actions 

const adyenApiServer = 'http://localhost:8080'

const shopServer = 'http://localhost:8082/api/carts/c10001/total'

// GET total from Shop server
export const setTotalAmount = () => async (dispatch) => {
  const response = await fetch(shopServer)
  dispatch(setAmount([await response.json(), response.status]))
}

// GET: Payment config from server
export const getAdyenConfig = () => async (dispatch) => {
  const response = await fetch(`${adyenApiServer}/api/config`)
  dispatch(config(await response.json()))
}

// POST: get paymentMethods 
export const getPaymentMethods = () => async (dispatch) => {
  const response = await fetch(`${adyenApiServer}/api/getPaymentMethods`, {
    method: "POST"
  })
  dispatch(paymentMethods([await response.json(), response.status]))
}

// POST Init Payment 
export const initiatePayment = (data) => async (dispatch) => {
  const response = await fetch(`${adyenApiServer}/api/initiatePayment`, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    }
  })
  dispatch(payments([await response.json(), response.status]))
}

// POST: Submit Additional Details
export const submitAdditionalDetails = (data) => async (dispatch) => {
  const response = await fetch(`${adyenApiServer}/api/submitAdditionalDetails`, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json"
    }
  })
  dispatch(paymentDetails([await response.json(), response.status]))
}

export default slice.reducer;